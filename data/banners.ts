import BannerData from 'autogen/banners'

type BannerType = {
  slug: string, // Auto generated
  date: string, // Auto generated
  type: string, // Auto generated
  title: string,
  description: string,
  background?: string,
};

// TODO: Move this into the autogen for banners.ts
import banner1 from 'public/banner/banner1.jpg'
import banner2 from 'public/banner/banner2.jpg'
import banner3 from 'public/banner/banner3.jpg'
import banner4 from 'public/banner/banner4.jpg'
import banner5 from 'public/banner/banner5.jpg'
import shanghai from 'public/banner/shanghai.jpg'
import ces from 'public/banner/ces.png'
import embeddeworld from 'public/banner/embeddedworld.jpg'

let BannerImages = {
  'banner/banner1.jpg': banner1,
  'banner/banner2.jpg': banner2,
  'banner/banner3.jpg': banner3,
  'banner/banner4.jpg': banner4,
  'banner/banner5.jpg': banner5,
  'banner/shanghai.jpg': shanghai,
  'banner/ces.png': ces,
  'banner/embeddedworld.jpg': embeddeworld,
}


const Banners: BannerType[] = BannerData;
const Images = BannerImages;

export { type BannerType, Banners, Images}
