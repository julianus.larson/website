import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import Link from "next/link";
import { useRouter } from "next/router";

import SOAFEELogo from "@/components/soafeelogo";
import { use } from "react";

interface MenuItem {
    name: string;
    children?: MenuItem[];
    url?: string;
    active?: string[] | string;
}

const aboutMenu: MenuItem[] = [
    { name: "Accessability", url: "/about/accessability" },
    { name: "Charter", url: "/about/charter" },
    { name: "Members", url: "/about/members" },
    { name: "Privacy", url: "/about/privacy" },
    { name: "Terms of Use", url: "/about/terms_of_use" },
];
const communityMenu: MenuItem[] = [
    { name: "Join", url: "/community/join" },
    { name: "Calendar", url: "/community/calendar" },
    { name: "GitLab", url: "https://gitlab.com/soafee" },
    { name: "LinkedIn", url: "https://www.linkedin.com/company/soafee" },
    { name: "Slack", url: "https://join.slack.com/t/soafee/shared_invite/zt-12e17668h-DttNpOtyFNi5H1udojYmtg" },
];

const postsMenu: MenuItem[] = [
    { name: "Blog", url: "/blog" },
    { name: "News", url: "/news" },
    { name: "Events", url: "/event" },
]

const documentsMenu: MenuItem[] = [
    { name: "Architecture", url: "https://architecture.docs.soafee.io" },
];

const headerMenu: MenuItem[] = [
    { name: "About", active: "/about", children: aboutMenu },
    { name: "Docs", children: documentsMenu },
    { name: "Posts", active: ["/blog", "/news", "/event"], children: postsMenu },
    { name: "Community", active: "/community", children: communityMenu },
];

const GenerateDropdown = (items: MenuItem[]) => {
    return items.map((item, index) =>
        <Link href={item.url?item.url:"#"} passHref key={index} legacyBehavior><NavDropdown.Item>{item.name}</NavDropdown.Item></Link>
    );
}

const GenerateMenu = (items: MenuItem[]) => {
    let route = useRouter().route;

    let isActive = (item: MenuItem, route: string) => {
        let paths = item.active ?? [];
        // Place item into an array if is just a string
        if(!Array.isArray(paths)) paths = [paths];
        // Return true if this menu entry contains the route
        return paths.findIndex((path) => route.startsWith(path)) != -1;
    }

    return items.map((item, index) => {
        let active = isActive(item, route);
        if(item.children != undefined) {
            return <NavDropdown title={item.name} active={active} key={index}>
                {GenerateDropdown(item.children)}
            </NavDropdown>
        } else {
            return <Link href={item.url?item.url:"#"} passHref key={index} legacyBehavior><Nav.Link active={active}>{item.name}</Nav.Link></Link>;
        }
    });
}


const Header = () => {
    return (
        <Navbar bg="dark" variant="dark" sticky="top" expand="sm">
            <Container>
                <Link href='/' passHref legacyBehavior>
                    <Navbar.Brand aria-label="SOAFEE Homepage">
                        <SOAFEELogo width={115} height={30} allowAnimate={false} alt="SOAFEE Homepage"></SOAFEELogo>
                    </Navbar.Brand>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        { GenerateMenu(headerMenu)}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;