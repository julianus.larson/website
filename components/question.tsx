import React from 'react'
import Accordion from 'react-bootstrap/Accordion';

type QProps = {
    children: React.ReactNode,
    q: string,
    n: string,
}

class Q extends React.Component<QProps> {
    render() {
        return <Accordion.Item eventKey={this.props.n}>
            <Accordion.Header>Q{this.props.n} {this.props.q}</Accordion.Header>
            <Accordion.Body>{this.props.children}</Accordion.Body>
        </Accordion.Item>
    }
}

type Props = {
    children: React.ReactElement<Q>
}

class Questions extends React.Component<Props> {
    render() {
        return <Accordion>
            {this.props.children}
        </Accordion>
    }

    static Q = Q
}

export default Questions;
