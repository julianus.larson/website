type LocaleDateProps = {
    date: string;
}

let LocaleDate = (props: LocaleDateProps) => {
    let d = new Date(props.date)

    // DateTimeFormatOptions does not seem to be defined
    // I think this is an issue with tsconfig.json
    // TODO: Fix this error, ignoring for now so we can proceed
    // @ts-ignore
    const options: DateTimeFormatOptions = {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
    }

    return <>{d.toLocaleDateString('default', options)}</>
}

export default LocaleDate