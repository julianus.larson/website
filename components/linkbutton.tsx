import { Button } from "react-bootstrap";

type Props = {
    url: string;
    children: React.ReactNode;
}

let LinkButton = (props: Props) => {
    return <a href={props.url}><Button style={{width: "100%"}}>{props.children}</Button></a>

}

export default LinkButton;
