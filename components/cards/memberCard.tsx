import { Card, Col, Ratio, Row } from "react-bootstrap";
import MemberLogo from "@/components/memberlogo";
import React from "react";
import OptionalCardLink from "./optionalcardlink";

type Props = {
    horizontal?: boolean
    memberId: string
    footer: React.ReactNode
    url?: string
    children: React.ReactNode
}

let HorizontalCardBody = (props: Props) => {
    return (
    <>
        <Card.Body>
            <Row>
                <Col sm={4} md={3} lg={2}>
                    <Ratio aspectRatio={'16x9'}>
                        <MemberLogo member={props.memberId} alt={props.memberId} noLink></MemberLogo>
                    </Ratio>
                </Col>
                <Col sm={8} md={9} lg={10}>
                    {props.children}
                </Col>
            </Row>
        </Card.Body>
        <Card.Footer>{props.footer}</Card.Footer>
    </>)
}

let VerticalCardBody = (props: Props) => {
    return (
    <>
        <Card.Body>
            <Ratio aspectRatio={'16x9'}>
                <MemberLogo member={props.memberId} alt={props.memberId} noLink></MemberLogo>
            </Ratio>
            {props.children}
        </Card.Body>
        <Card.Footer>{props.footer}</Card.Footer>
    </>)
}


let MemberCard = (props: Props) => {
    return (
    <Card className="memberCard cardHover">
        <OptionalCardLink url={props.url}>
            {props?.horizontal ? HorizontalCardBody(props) : VerticalCardBody(props)}
        </OptionalCardLink>
    </Card>)
}

export default MemberCard;