import Image from 'next/image'
import Link from 'next/link';
import { Members, Member } from 'data/members';

type Props = {
    member?: Member | string,
    alt?: string,
    useInternal?: boolean,
    noLink?: boolean,
}

const MemberLogo = (props: Props) => {
    let getMember = (member?: Member | string) => {
        if(typeof(member) === 'string') {
            return Members.find((item: Member) => item.id == member);
        } else  {
            // The website will be unique, search for the member by this id
            return Members.find((item: Member) => item.website == member?.website);;
        } 
    }

    let member = getMember(props?.member);
    if(member == undefined) {
        console.error("Member not found %s", props.member);
        return <></>
    }

    let image = <Image src={member.imageData} alt={props.alt ? props.alt : member.name} className="memberlogo"></Image>

    if(props.noLink)
        return <>{image}</>
    else if(props?.useInternal == true) {
        return <Link href={"/about/members/" + member.id} passHref>{image}</Link>;
    } else {
        return <a href={member.website}>{image}</a>
    }
}

export default MemberLogo;