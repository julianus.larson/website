import { Col, Container, Ratio, Row } from "react-bootstrap";
import { Member, Members, MemberType } from 'data//members';
import MemberLogo from "./memberlogo";

type Props = {
    type: MemberType
    columns: number
    minColumns?: number
}

const LogoGrid = (props: Props) => {

    let members = Members
        .filter((member: Member) => member.type == props.type)
        .sort((a: Member, b: Member) => a.name.localeCompare(b.name))
        //.slice(0, 20)
        .map((member: Member, index: number) =>
            <Col key={index} style={{padding: "2%"}}>
                <Ratio aspectRatio={'16x9'}>
                    <MemberLogo key={index} member={member.id} useInternal={true}></MemberLogo>
                </Ratio>
            </Col>
        );

    
    return <Container><Row xs={props.minColumns||1} sm={props.columns} className='justify-content-center'>{members}</Row></Container>
}

export default LogoGrid;