import { Carousel as C} from "react-bootstrap"
import { BannerType, Banners } from 'data/banners';
import Banner from '@/components/banner'


type Props = {
    // The types to source the banner from.  This is derived from the frontmatter.
    types: string[],
    // The count of each type that we want to show
    count?: number,
    // A list of pinned posts that we want to have banners for.  This is in addition to the count.
    pinned?: string[],
}

const generateItems = (props: Props) => {
    // Descending sort by date
    let sortByDate = (a: BannerType, b:BannerType) => {
        return new Date(b.date).getTime() - new Date(a.date).getTime();
    }

    // Sort by date
    let sortedBanners = Banners.sort(sortByDate);
    // For each type in props
    let banners = props.types.map(type => {
        // Sort by type
        return sortedBanners.filter(banner => {
                if(Array.isArray(banner.type))
                    return banner.type.indexOf(type) > -1;
                else
                    return banner.type == type;
            })
            // Limit to the required number
            .slice(0, props?.count ?? 2);
        })
    // Merge arrays
    .reduce((a, b) => a.concat(b))
    // Sort by date again
    .sort(sortByDate)
    // Return array
    return banners;
}


const Carousel = (props: Props) => {
    let items = generateItems(props);
    return (
    <C>
        {items.map((item, index) =>
            <C.Item key={index}>
                <Banner data={item}></Banner>
            </C.Item>
        )}
    </C>)
}

export default Carousel;