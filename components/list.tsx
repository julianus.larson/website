import * as React from "react"
import { Col, Row } from "react-bootstrap"
import ImageCard from "./cards/imageCard";

import MemberCard from "./cards/memberCard";
import SimpleCard from "./cards/simpleCard";
import LocaleDate from "./localedate";

type Props = {
    cardType: string,
    data: any,
    width?: number,
}

type Item = {
    frontmatter: {
        cardType?: string,
        title: string,
        description: string,
        memberId: string,
        date: string,
        cardImage?: string,
    }
    content?: string,
    slug: string,
    footer?: string,
    memberId?: string,
}

const List = (props: Props) => {
    let width = props.width||1;

    // Generate the raw cards
    let cards = props.data.map((item: Item, index: number) => {
        let cardType = item?.frontmatter?.cardType || props?.cardType || 'simpleCard';
        switch(cardType) {
            case 'eventCard':
                return (
                <MemberCard memberId={item.memberId||""} footer={item.footer} url={item.slug}>
                    <div dangerouslySetInnerHTML={{ __html: item?.content||"" }} />
                </MemberCard>)
            case 'memberCard': {
                let date = <LocaleDate date={item.frontmatter.date}></LocaleDate>;
                return (
                <MemberCard horizontal memberId={item.frontmatter.memberId} footer={date} url={item.slug}>
                    <h5>{item.frontmatter.title}</h5>
                    <p>{item.frontmatter.description}</p>
                </MemberCard>)
            }
            case 'imageCard': {
                let date = <LocaleDate date={item.frontmatter.date}></LocaleDate>;
                return (
                    <ImageCard horizontal image={item.frontmatter.cardImage||""} footer={date} url={item.slug}>
                        <h5>{item.frontmatter.title}</h5>
                        <p>{item.frontmatter.description}</p>
                    </ImageCard>
                )
            }
            case 'simpleCard': {
                let date = <LocaleDate date={item.frontmatter.date}></LocaleDate>;
                return (
                <SimpleCard url={item.slug} footer={date}>
                    <h5>{item.frontmatter.title}</h5>
                    <p>{item.frontmatter.description}</p>
                </SimpleCard>)
            }
            default:
                console.log("Unknown card type " + cardType);
        }
    });

    if(width > 1) {
        // Add unique keys to all cards
        cards = cards.map((card: React.ReactNode, index: number) => {
            return <Col key={index}>{card}</Col>
        });
        return <Row sm={width} xs={1}>{cards}</Row>
    } else {
        return cards;
    }
}

export default List;