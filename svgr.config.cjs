module.exports = {
    // Strip width/height from svg leaving just viewBox to allow css scaling
    dimensions: false,
};