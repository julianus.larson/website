import {globSync} from 'glob';
import matter from 'gray-matter';
import fs from 'node:fs';
import path from 'node:path';

let imports = `
`;

let exports = `

export default BannerData;
`;

let generateBanners = async(outputDir) => {
    // Ensure the output directory exists
    if (!fs.existsSync(outputDir)){
        fs.mkdirSync(outputDir);
    }
    
    // Find all .mdx pages in pages
    let banners = globSync('pages/**/*.mdx')
    .map(file => {
        let data = matter(fs.readFileSync(file)).data;
        if(data) {
            // Add a 'slug' to the banner
            data.slug = file.substring(6).replace(/\.mdx$/,'');
        }
        return data;
    })
    // Only include the frontmatter items that have a banner component
    .filter(item => item!=undefined && item['banner'])



    // Extract banner components and add additional metadata
    .map(item => {
        // TODO: validate that banner has the correct signature
        let banner = item.banner;
        // Use the page title if the banner does not define one.
        banner.title = item.banner?.title ?? item.title;
        // Use the page description if the banner does not define one
        banner.description = item.banner?.description ?? item.description;
        banner.type = item.type;
        banner.date = item.date;
        banner.slug = item.slug;

        return banner;
    })

    let out = fs.createWriteStream(path.resolve(outputDir, 'banners.ts'));

    // Add boilerplate imports
    out.write(imports);
    // Add the member array
    out.write('const BannerData = [\n')
    banners.map(item => out.write('  ' + JSON.stringify(item) + ',\n'));
    out.write('];\n');
    // Add the exports definition
    out.write(exports);
}

export default generateBanners;