import {globSync} from 'glob';
import matter from 'gray-matter';
import fs from 'node:fs'
import * as path from 'path';

let imports = `
import MemberType from 'data/membertype'

`;

let exports = `

export default MemberData;
`;

let generateMembers = async (outputDir) => {
    // Ensure the output directory exists
    if (!fs.existsSync(outputDir)) {
        fs.mkdirSync(outputDir);
    }

    // Find all .mdx pages in pages/about/members
    let fm = globSync('pages/about/members/*.mdx')
        // Extract frontmatter
        .map((file) => {
            let member = matter(fs.readFileSync(file)).data.member;
            // Extract id from filename, this is guaranteed to be unique as they are all in the same directory
            member.id = path.basename(file, '.mdx');
            member.slug = file.substring(5).replace(/\.mdx$/, '');
            return member;
        });

    // Generate members.ts

    // Generate image imports
    let imageImports = fm.map((data) => {
        return `import ${data.id}Logo from 'public/${data.logo}'`
    });
    // Generate data
    let memberArray = fm.map((d) => {
        return `{ id: '${d.id}', type: ${d.type}, name: '${d.name}', imageData: ${d.id}Logo, website: '${d.website}', slug: ' ${d.slug} '}`
    });

    let out = fs.createWriteStream(path.resolve(outputDir, 'members.ts'));

    // Add boilerplate imports
    out.write(imports);
    // Add image imports (so next.js can statically analyze)
    imageImports.map(line => out.write(line + '\n'))
    // Add the member array
    out.write('const MemberData = [\n')
    memberArray.map(line => out.write('  ' + line + ',\n'));
    out.write('];\n');
    // Add the exports definition
    out.write(exports);
}

export default generateMembers;