import '../styles/main.scss'
import type { AppProps } from 'next/app'
import Layout from '@/components/layout'
import GoogleAnalytics from '@/components/google_analytics'
import { SSRProvider } from 'react-bootstrap'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <SSRProvider>
      <GoogleAnalytics />
      <Layout {...pageProps}>
        <Component {...pageProps} />
      </Layout>
    </SSRProvider>
  )
}

export default MyApp
