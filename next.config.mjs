import nextMDX from "@next/mdx";
import remarkGfm from "remark-gfm";
import remarkFrontmatter from 'remark-frontmatter';
import remarkMdxFrontmatter from 'remark-mdx-frontmatter';
import recmaNextjsStaticProps from 'recma-nextjs-static-props'
import rehypeSlug from 'rehype-slug'


/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,

  i18n: {
    locales: ["en"],
    defaultLocale: "en",
  },

  webpack: (config,  { buildId, dev, isServer, defaultLoaders, nextRuntime, webpack }) => {
    // Enable use of SVG files are react object
    // enables syntax like 'import MySVGVile from "image/file.svg"'
    config.module.rules.push({
      test: /\.svg$/,
      issuer: /\.(js|ts|md)x?$/,
      use: ["@svgr/webpack"]
    });

    return config;

  }
}

const withMDX = nextMDX({
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [
      remarkGfm,
      remarkFrontmatter,
      [remarkMdxFrontmatter, {name: 'frontmatter'}],
    ],
    rehypePlugins: [rehypeSlug],
    recmaPlugins: [recmaNextjsStaticProps],
    // If you use `MDXProvider`, uncomment the following line.
    // providerImportSource: "@mdx-js/react",
  },
});

let mdxConfig = withMDX({
  // Append the default value with md extensions
  pageExtensions: ['ts', 'tsx', 'js', 'jsx', 'md', 'mdx'],
  // Append existing nextConfig
  ...nextConfig
})

export default mdxConfig
